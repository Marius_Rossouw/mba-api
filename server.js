// --------- server.js for emba------------
  
var express = require('express');
var http = require('http');
var path = require('path');
var mongoclient = require('mongodb').MongoClient;
var setup = require('./includes/setup.js');

//create a mongo connection instance
var mongodbconnection;
mongoclient.connect(setup.mongo_db,function(err,data){
    mongodbconnection = data;
});

var logreq = function(req, res, next) {
    //log stuff of the request here
    next();
};
var passInDB = function(req, res, next) {
    req.db = mongodbconnection;
    next();
};
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);    
    next();
};
var dontcache = function(req, res, next) {
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
    next();
};

//create express instance and set it up
var app = express();
app.set('port', setup.server_port);

//middlewarea: http://expressjs.com/guide/using-middleware.html
app.use(express.favicon(path.join(__dirname,"/favicon.ico")));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
//app.use(logreq);
app.use(allowCrossDomain);
app.use('/files',express.static(__dirname + '/uploads')); //will look for a static file here. If not will continue.
app.use(dontcache);
app.use(passInDB);
app.use(app.router);

// =============== route to functions =====================

function default_message(req, res){
   //res.send(200,"<h3>Memberbase API</h3>");
   res.send(404,"");
}

app.get('/', default_message); 

var profile = require('./routes/profile.js');  // includes the file
app.post('/login', profile.login);
app.post('/register', profile.register_person);

app.put('/profile/:id', profile.profile_update_one);
app.delete('/profile/:id', profile.profile_delete_one);

var persons = require('./routes/persons.js');
app.get('/persons', persons.persons_list);
app.get('/persons/:id', persons.persons_get_one);
app.put('/persons/:id', persons.persons_update_one);
app.delete('/persons/:id', persons.persons_delete_one);

var websites = require('./routes/websites.js');
app.get('/websites', websites.websites_list);
app.get('/websites/:id', websites.websites_get_one);
app.post('/websites', websites.websites_add_one);
app.put('/websites/:id', websites.websites_update_one);
app.delete('/websites/:id', websites.websites_delete_one);

var emails = require('./routes/emails.js');
app.get('/emails', emails.emails_list);
app.get('/emails/:id', emails.emails_get_one);
app.post('/emails', emails.email_add_one);
app.put('/emails/:id', emails.emails_update_one);
app.delete('/emails/:id', emails.emails_delete_one);

var pdfs = require('./routes/pdfs.js');
app.get('/pdfs', pdfs.pdfs_list);
app.get('/pdfs/:id', pdfs.pdfs_get_one);
app.post('/pdfs', pdfs.pdf_add_one);
app.put('/pdfs/:id', pdfs.pdfs_update_one);
app.delete('/pdfs/:id', pdfs.pdfs_delete_one);

var contact = require('./routes/contact.js');  // includes the file
app.post('/contact', contact.email);


// ============== Start listening ===========================

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

