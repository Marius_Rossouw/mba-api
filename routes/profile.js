var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
//var ISODate = require('mongodb').ISODate;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

exports.login = function(req, res) {
// console.log('req=' + Object.keys(req));
  async.waterfall([
    function(callback) {
      var projection = {password:false };
      var filter = {'email':req.body.auth_identifier,'password':req.body.auth_password};
      req.db.collection('persons').find(filter,projection).toArray(callback);
    },
    function(data,callback) {
      if (!data || data.length === 0) {
        callback({"http_code":"404","message":"incorrect credentials - (22)"},{});
      } else {
        callback(null,data[0]);
      }
    },
    function(data,callback) {
      if (data._id) {
        var resp = {};
        resp.token = data._id;   // Why the _id twice?
        resp.person_id = data._id;
        resp.username = data.firstname + ' ' + data.lastname;  // response as name and surname
        resp.email = data.email;    // add the email to the response



        if(!data.superuser){     // ensure the superuser is either false of true and not undefined
          resp.superuser = false;
        } else {
          resp.superuser = data.superuser;
        }
      
      callback(null,resp);
    } else {
      callback({"http_code":"404","message":"incorrect credentials  (45)"},{});
    }
  }
  ],
  function(err,data) {
    if (err) {
      if (err.http_code) {
        res.send(err.http_code,err);
      } else {
        var api_err = {"http_code":"500","message":"system error", "dump":err};
        res.send(500,api_err);
      }
    } else {
      res.send(201,data);
    }
  });
};

exports.register_person = function(req, res) {
  //console.log(req.params);
  //console.log(req.query);

  async.waterfall([
    function(callback) {
      var filter = {'email':req.body.email};
      req.db.collection('persons').find(filter).toArray(callback);
    },
    function(data,callback) {
      if (data && data.length > 0) {
        callback({"http_code":"403","message":"email exists"},{});
      } else {
        callback(null,{});
      }
    },
    function(data,callback) {
      if (req.body.firstname && req.body.lastname && req.body.email && req.body.password) {
        req.body.create_date = new Date();
        req.db.collection('persons').insert(req.body,callback);
      } else {
        callback({"http_code":"403","message":"incomplete data"},{});
      }
    }
    ],
    function(err,data) {
      if (err) {
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(201);
      }
    });
};

exports.profile_update_one = function(req, res){
  console.log("profile_update_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('persons').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
};

exports.profile_delete_one = function(req, res){
  console.log("profile_update_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var u = {$set:{deleted:true}};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('persons').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
};

