// mba-api  emails.js    
var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");
// ----------------------------------------------------------

exports.emails_list = function(req, res){    // request, response
  console.log("emails_list");
    // console.log("req=" + Object.keys(req.body));

    async.waterfall([
      function(callback) {
        var projection = {
          _id: true,
          webName: true,
          webOwner: true,
          webUrl: true,
          webCat: true,       
          webRepo: true,
          apiRepo: true,    
          webSvrAdr: true,
          apiSvrAdr: true,
          apiPort: true
        };

      // console.log(JSON.stringify(req.query)); 
 
        var superuser=null;  
        // it will pick up its appropriate typeof along the way
        var loginEmail="";

        superuser = req.query.spr;   

        loginEmail = req.query.em; 

      // console.log("loginEmail= " + loginEmail); 

      var filter = {};
      // -------------------------------------
      // The procedure below works best with the switch method because
      // the typeof of superuser is freaky. Attempts have also been made 
      // to use it as a boolean but was unsuccessful
      switch (superuser) {

        case "true":
        filter = {deleted:{$ne:true}};
        break;

        case "false":
        filter = {deleted:{$ne:true}, webOwner:loginEmail};
        break;

        default: 
        console.log("Invalid superuser value: " + superuser);
        break;
      }

      req.db.collection('emails').find(filter,projection).sort({webName:1}).toArray(callback);
    }
  ],   // end waterfall

  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      for (var i=0, l=data.length;i<l;i++) {
        data[i].name = utils.concat_names([data[i].webName,data[i].webRepo]);
      }
      res.send(200,data);
    }
  });
};   // end exports.emails_list
// ---------------------------------------------------

exports.emails_get_one = function(req, res){

  // console.log("emails_get_one");
  var id = req.params.id;

  var resp = {};

  async.waterfall([
    function(callback) {
      // console.log(id);
      var query = {'_id':ObjectID(id)};
      req.db.collection('emails').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"email not found", "dump":{}});
        return;
      }
      resp = data[0];
      callback(null,{});
    }
    ],
    function(err,data) {
      if (err) {
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resp);
      }
    });
};
// -------------------------------------------------------------
// exports.email_add_one = function(req, res) {

//   console.log("at api emails.js email_add_one");
//   var id = req.params.id;

//   async.waterfall([
//     function(callback) {
//       var filter = {'et_name':req.body.et_name};
//       req.db.collection('emails').find(filter).toArray(callback);
//     },
//     function(data,callback) {
//       if (data && data.length > 0) {
//         callback({"http_code":"403","message":"template exists"},{});
//       } else {
//         callback(null,{});
//       }
//     },
//     function(data,callback) {
//       if (req.body.et_name && req.body.et_product_name &&
//         req.body.et_website_name && req.body.et_subject && 
//         req.body.et_from_address && req.body.et_smtp_id && 
//         req.body.et_body) {

//         req.body.create_date = new Date();
//       req.db.collection('emails').insert(req.body,callback);
//     } else {
//       callback({"http_code":"403","message":"incomplete data"},{});
//     }
//   }
//   ],
//   function(err,data) {
//     if (err) {
//       if (err.http_code) {
//         res.send(err.http_code,err);
//       } else {
//         var api_err = {"http_code":"500","message":"system error", "dump":err};
//         res.send(500,api_err);
//       }
//     } else {
//       res.send(201);
//     }
//   });
// };


exports.email_add_one = function(req, res){
  console.log("email_add_one");
  var result = {};
  async.waterfall([

    function(callback) {
      var filter = {'et_name':req.body.et_name};
      req.db.collection('emails').find(filter).toArray(callback);
    },
    function(data,callback) {
      if (data && data.length > 0) {
        callback({"http_code":"403","message":"template exists"},{});
      } else {
        callback(null,{});
      }
    },

    function(data,callback) {
        req.body.create_date = new Date();
        req.body.update_date = new Date();
        var o = req.body;
        if (o.created_by){
          o.created_by = ObjectID(o.created_by);
        }
        req.db.collection('emails').insert(o,callback);
    },

    function(data,callback) {
      console.log(JSON.stringify(data));
      result._id = data._id;
      callback(null,{});
    }
  ],

  function(err,data) {
    if (err) {
      if (err.http_code) {
        res.send(err.http_code,err);
      } else {
        var api_err = {"http_code":"500","message":"system error", "dump":err};
        res.send(500,api_err);
      }
    } else {
      res.send(201,result);
    }
  });
};
  

// ----------------------------------------------------------------

exports.emails_update_one = function(req, res){
  console.log("emails_update_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('emails').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
};
// --------------------------------------------------------------------

exports.emails_delete_one = function(req, res){
  console.log("emails_delete_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var u = {$set:{deleted:true}};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('emails').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
}; 