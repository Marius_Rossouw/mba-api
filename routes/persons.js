//   persons.js

var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");
 
exports.persons_list = function(req, res){  // request, response
  console.log("exports.persons_list");

  async.waterfall([
    function(callback) {
      var projection = {
        _id: true,
        firstname: true,
        lastname: true,
        email: true
      };
      var filter = {deleted:{$ne:true}};

      req.db.collection('persons').find(filter,projection).sort({firstname:1}).toArray(callback);
    }
  ],   // end waterfall

  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      for (var i=0, l=data.length;i<l;i++) {
        data[i].name = utils.concat_names([data[i].firstname,data[i].lastname]);
      }
      res.send(200,data);  // Cannot use res.write or res.end here
    }
  }
  );     // end waterfall
};       // end exports.persons_list

exports.persons_get_one = function(req, res){

  console.log("exports.persons_get_one");
  // console.info("---  Console Report  ---");


  var id = req.params.id;

  var resp = {};

  async.waterfall([
    function(callback) {
      var query = {'_id':ObjectID(id)};
      req.db.collection('persons').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"person not found", "dump":{}});
        return;
      }
      resp = data[0];

        // console.log('>' + Object.keys(resp) + '<');

      callback(null,{});
    }
    ],
    function(err,data) {
      if (err) {
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resp);
      }
    });
};         // end exports.persons_get_one

exports.persons_update_one = function(req, res){
  console.log("exports.persons_update_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  delete o._id;
  var u = {$set:o};

  var resultdata = {}; // This doesnt do anything
        // console.log('>'+Object.keys(resultdata)+'<');
  async.waterfall([
    function(callback) {
      req.db.collection('persons').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {

        res.send(200,resultdata);
      }
    });
};  // end exports.persons_update_one

exports.persons_delete_one = function(req, res){
  console.log("exports.persons_delete_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var u = {$set:{deleted:true}};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('persons').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
};   // end  exports.persons_delete_one