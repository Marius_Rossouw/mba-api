// mba-api  websites.js    
var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");
// ----------------------------------------------------------

exports.websites_list = function(req, res){    // request, response
  console.log("websites_list");
    // console.log("req=" + Object.keys(req.body));

    async.waterfall([
      function(callback) {
        var projection = {
          _id: true,
          webName: true,
          webOwner: true,
          webUrl: true,
          webCat: true,       
          webRepo: true,
          apiRepo: true,    
          webSvrAdr: true,
          apiSvrAdr: true,
          apiPort: true
        };

      // console.log(JSON.stringify(req.query)); 
 
        var superuser=null;  
        // it will pick up its appropriate typeof along the way
        var loginEmail="";

        superuser = req.query.spr;   

        loginEmail = req.query.em; 

      // console.log("loginEmail= " + loginEmail); 

      var filter = {};
      // -------------------------------------
      // The procedure below works best with the switch method because
      // the typeof of superuser is freaky. Attempts have also been made 
      // to use it as a boolean but was unsuccessful
      switch (superuser) {

        case "true":
        filter = {deleted:{$ne:true}};
        break;

        case "false":
        filter = {deleted:{$ne:true}, webOwner:loginEmail};
        break;

        default: 
        console.log("Invalid superuser value: " + superuser);
        break;
      }

      req.db.collection('websites').find(filter,projection).sort({webName:1}).toArray(callback);
    }
  ],   // end waterfall

  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      for (var i=0, l=data.length;i<l;i++) {
        data[i].name = utils.concat_names([data[i].webName,data[i].webRepo]);
      }
      res.send(200,data);
    }
  });
};   // end exports.websites_list
// ---------------------------------------------------

exports.websites_get_one = function(req, res){

  // console.log("websites_get_one");
  var id = req.params.id;

  var resp = {};

  async.waterfall([
    function(callback) {
      // console.log(id);
      var query = {'_id':ObjectID(id)};
      req.db.collection('websites').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"website not found", "dump":{}});
        return;
      }
      resp = data[0];
      callback(null,{});
    }
    ],
    function(err,data) {
      if (err) {
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resp);
      }
    });
};
// -------------------------------------------------------------
exports.websites_add_one = function(req, res) {

  console.log("websites_add_one");

  async.waterfall([
    function(callback) {
      var filter = {'webName':req.body.webName};
      req.db.collection('websites').find(filter).toArray(callback);
    },
    function(data,callback) {
      if (data && data.length > 0) {
        callback({"http_code":"403","message":"Website exists"},{});
      } else {
        callback(null,{});
      }
    },
    function(data,callback) {

      if (req.body.webName && req.body.webOwner && 
        req.body.webUrl &&  req.body.webCat &&
        req.body.webRepo && req.body.apiRepo && 
        req.body.webSvrAdr && req.body.apiSvrAdr && 
        req.body.apiPort) {

        req.body.create_date = new Date();
      req.db.collection('websites').insert(req.body,callback);
    } else {
      callback({"http_code":"403","message":"incomplete data"},{});
    }
  }
  ],
  function(err,data) {
    if (err) {
      if (err.http_code) {
        res.send(err.http_code,err);
      } else {
        var api_err = {"http_code":"500","message":"system error", "dump":err};
        res.send(500,api_err);
      }
    } else {
      res.send(201);
    }
  });
};

// ----------------------------------------------------------------

exports.websites_update_one = function(req, res){
  console.log("websites_update_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('websites').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
};
// --------------------------------------------------------------------

exports.websites_delete_one = function(req, res){
  console.log("websites_delete_one");

  var id = req.params.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var u = {$set:{deleted:true}};

  var resultdata = {};
  async.waterfall([
    function(callback) {
      req.db.collection('websites').update(query,u,options,function(err){
        callback(err,{});
      });
    },
    ],
    function(err,data) {
      if (err) {
        console.error(err);
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system.error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resultdata);
      }
    });
}; 