/*  ----  utils.js  -----

*/
exports.concat_names = concat_names;

function concat_names(a){
  var name = '';
  for (var i=0;i< a.length;i++){
    if (a[i] && a[i].length > 0){
      if (name.length > 0){
        name = name + ' ';
      }
      name = name + a[i];
    }
  }
  return name;
}